Le lien du JSFiddle est dans le fichier LienJSFiddle.txt

Le code source de la consigne 2 est dans AD2019

Le code source de la consigne 3 est dans ZOO_MANAGER

Le fichier Reponses_aux_questions.txt contient les r�ponses aux questions pos�es dans le sujet de TP. 


************************************************************
		Le service ZOO_MANAGER

Notre service REST dispose des fonctionnalit�s suivantes : 

PUT		/animals			?????? -> Cage (expliqu� dans le rapport)
DELETE		/animals

POST		/animals/{animal_id}		Animal
PUT		/animals/{animal_id}		Animal
DELETE		/animals/{animal_id}

GET		/find/byName/{name}

GET		/find/at/{pos}

GET		/find/near/{pos}

GET		/animals/{animal_id}/wolf

GET		/center/journey/from/{pos}

En plus des m�thodes demand�es pour le projet, j'ai rajout� d'autres m�thodes utiles : 

POST		/cages				Cage	Permet d'ajouter une nouvelle cage � notre centre
DELETE		/cages/{cageName}			Permet de vider une cage de notre centre, r�f�renc�e par son nom

Le sc�nario d�crit dans le fichier Sc�nario.pdf a �t� �x�cut� sur l'API. Plus de d�tails dans le rapport.
