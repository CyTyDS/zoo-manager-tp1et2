package tp.rest;

import tp.model.Animal;
import tp.model.Cage;
import tp.model.Center;
import tp.model.Position;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.util.JAXBSource;
import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.http.HTTPBinding;

import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Map;
import java.util.UUID;

public class MyClient {
    private Service service;
    private JAXBContext jc;

    private static final QName qname = new QName("", "");
    private static final String url = "http://127.0.0.1:8084";

    public MyClient() {
        try {
            jc = JAXBContext.newInstance(Center.class, Cage.class, Animal.class, Position.class);
        } catch (JAXBException je) {
            System.out.println("Cannot create JAXBContext " + je);
        }
    }

    //PUT /animals ?????? -> Cage
    public void modify_animals(Cage cages) throws JAXBException {
        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/animals");
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "PUT");
        Source result = dispatcher.invoke(new JAXBSource(jc, cages));
        printSource(result);
    }
    
    //POST /cages Cage
    public void add_cage(Cage cages) throws JAXBException {
        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/cages");
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "POST");
        Source result = dispatcher.invoke(new JAXBSource(jc, cages));
        printSource(result);
    }
    
    //DELETE /cages/{cageName}
    public void delete_cage(String cages) throws JAXBException {
    	service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/cages/" + cages);
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "DELETE");
        Source result = dispatcher.invoke(new JAXBSource(jc, new Animal()));
        printSource(result);
    }
    
    //POST /animals Animal
    public void add_animal(Animal animal) throws JAXBException {
        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/animals");
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "POST");
        Source result = dispatcher.invoke(new JAXBSource(jc, animal));
        printSource(result);
    }
    
    //DELETE /animals
    public void delete_All() throws JAXBException {
        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/animals");
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "DELETE");
        Source result = dispatcher.invoke(new JAXBSource(jc, new Animal()));
        printSource(result);
    }
    
    //DELETE /animals/{animal_id}
    public void suppr_id(String id) throws JAXBException {
        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/animals/" + id);
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "DELETE");
        Source result = dispatcher.invoke(new JAXBSource(jc, new Animal()));
        printSource(result);
    }
    
    //GET /find/byName/{name}
    public void search_name(String name) throws JAXBException {
        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/find/byName/" + name);
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "GET");
        Source result = dispatcher.invoke(new JAXBSource(jc, new Animal()));
        printSource(result);
    }
    
    //GET /find/near/{pos}
    public void search_near(String pos) throws JAXBException {
        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/find/near/" + pos);
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "GET");
        Source result = dispatcher.invoke(new JAXBSource(jc, new Animal()));
        printSource(result);
    }
    
    //GET /find/at/{pos}
    public void search_at(String pos) throws JAXBException {
        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/find/at/" + pos);
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "GET");
        Source result = dispatcher.invoke(new JAXBSource(jc, new Animal()));
        printSource(result);
    }
    
    //GET /animals/{animal_id}/wolf
    public void wolfram_animal(String pos) throws JAXBException {
        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/animals/" + pos + "/wolf");
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "GET");
        Source result = dispatcher.invoke(new JAXBSource(jc, new Animal()));
        printSource(result);
    }
    
    //GET /center/journey/from/{pos}
    public void trajet_to(String pos) throws JAXBException {
        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/center/journey/from/" + pos);
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "GET");
        Source result = dispatcher.invoke(new JAXBSource(jc, new Animal()));
        printSource(result);
    }
    
    //GET /animals
    public void print_all() throws JAXBException {
        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/animals");
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "GET");
        Source result = dispatcher.invoke(new JAXBSource(jc, new Animal()));
        
        try {
        	FileOutputStream out = new FileOutputStream("test.txt", true);
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer();
            transformer.transform(result, new StreamResult(out));
            out.write("\n".getBytes());
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void printSource(Source s) {
        try {
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer();
            transformer.transform(s, new StreamResult(System.out));
            System.out.println();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static void main(String args[]) throws Exception {
        MyClient client = new MyClient();
        
        //PRINT
        client.print_all();
        
        //Supprimez tous les animaux
        client.delete_All();
        
      //PRINT
        client.print_all();
        
        //AJOUTER A 
        client.add_cage(new Cage(
                "rouen",
                new Position(49.443889d, 1.103333d),
                25,
                new LinkedList<>(Arrays.asList())
        ));
        client.add_animal(new Animal("Panda", "rouen", "Panda", UUID.randomUUID()));
        client.add_cage(new Cage(
                "paris",
                new Position(48.856578d, 2.351828d),
                25,
                new LinkedList<>(Arrays.asList())
        ));
        client.add_animal(new Animal("Hocco unicorne", "paris", "Hocco unicorne", UUID.randomUUID()));
        
      //PRINT
        client.print_all();
        
        //Modifiez l'ensemble des animaux par
        client.modify_animals(new Cage(
                "transport",
                new Position(49.443889d, 1.103333d),
                25,
                new LinkedList<>(Arrays.asList(new Animal("Lagotriche à queue jaune", "rouen", "Lagotriche à queue jaune", UUID.randomUUID())))
        ));
        
      //PRINT
        client.print_all();
        
        client.add_cage(new Cage(
                "somalie",
                new Position(2.333333d, 48.85d),
                25,
                new LinkedList<>(Arrays.asList())
        ));
        client.add_animal(new Animal("Océanite de Matsudaira", "somalie", "Océanite de Matsudaira", UUID.randomUUID()));
        
        client.add_animal(new Animal("Ara de Spix", "rouen", "Ara de Spix", UUID.fromString("98c31340-9a35-48a0-915f-2d2a22ad8b9c")));

        client.add_cage(new Cage(
                "bihorel",
                new Position(49.455278d, 1.116944d),
                25,
                new LinkedList<>(Arrays.asList())
        ));
        client.add_animal(new Animal("gala", "bihorel", "Galago de Rondo", UUID.fromString("ce9a9627-b5c7-404e-8ed3-4a7b34d82770")));
        
        client.add_cage(new Cage(
                "londres",
                new Position(51.504872d, 1.116944d),
                25,
                new LinkedList<>(Arrays.asList())
        ));
        client.add_animal(new Animal("Palette des Sulu", "londres", "Palette des Sulu", UUID.randomUUID()));
        
        client.add_animal(new Animal("Kouprey", "paris", "Kouprey", UUID.randomUUID()));
        
        client.add_animal(new Animal("Tuit-­tuit", "paris", "Tuit-­tuit", UUID.randomUUID()));
        
        client.add_cage(new Cage(
                "canada",
                new Position(43.2d, 80.38333d),
                25,
                new LinkedList<>(Arrays.asList())
        ));
        client.add_animal(new Animal("Saïga", "canada", "Saïga", UUID.fromString("5f67ab54-c7b0-4ed4-adf2-0651e22b5c8b")));
        
        client.add_cage(new Cage(
                "porto-­vecchio",
                new Position(41.5895241d, 9.2627d),
                25,
                new LinkedList<>(Arrays.asList())
        ));
        client.add_animal(new Animal("Inca de Bonaparte", "porto-­vecchio", "Inca de Bonaparte", UUID.randomUUID()));
        
      //PRINT
        client.print_all();
        
        client.add_cage(new Cage(
                "montreux",
                new Position(46.4307133d, 6.9113575d),
                25,
                new LinkedList<>(Arrays.asList())
        ));
        client.add_animal(new Animal("Râle de Zapata", "montreux", "Râle de Zapata", UUID.randomUUID()));
        
        client.add_cage(new Cage(
                "villers­-bocage",
                new Position(50.0218d, 2.3261d),
                25,
                new LinkedList<>(Arrays.asList())
        ));
        client.add_animal(new Animal("Rhinocéros de Java", "villers­-bocage", "Rhinocéros de Java", UUID.randomUUID()));
        
        for (int i = 1; i <= 101; ++i) {
        	client.add_animal(new Animal("Dalmatien", "usa", "Dalmatien", UUID.randomUUID()));
        }
        
      //PRINT
        client.print_all();
        
        //Supprimez tous les animaux de Paris
        client.delete_cage("paris");
        
      //PRINT
        client.print_all();
        
        //search galago gala
        client.search_name("gala");
        
        //suppr galago ce9a9627-b5c7-404e-8ed3-4a7b34d82770
        client.suppr_id("ce9a9627-b5c7-404e-8ed3-4a7b34d82770");
        
        //suppr galago 2
        client.suppr_id("ce9a9627-b5c7-404e-8ed3-4a7b34d82770");
        
      //PRINT
        client.print_all();
        
        //search near Rouen
        client.search_near("49.44,1.1");
        //search at Rouen
        client.search_at("49.443889,1.103333");
        
        //Wolfram Alpha du Saïga
        client.wolfram_animal("5f67ab54-c7b0-4ed4-adf2-0651e22b5c8b");
        //Wolfram Alpha de l'Ara de Spix
        client.wolfram_animal("98c31340-9a35-48a0-915f-2d2a22ad8b9c");
        
        //Trajet GraphHopper jusqu'au centre de Somalie
        client.trajet_to("2.333333,48.85");
        //Trajet GraphHopper jusqu'au centre de Londres
        client.trajet_to("51.504872,1.116944");
        
        //Supprimez tous les animaux
        client.delete_All();
        
      //PRINT
        client.print_all();
        
        System.out.println("Finished");
    }
}
